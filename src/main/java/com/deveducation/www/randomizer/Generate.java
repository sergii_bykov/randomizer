package com.deveducation.www.randomizer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Generate {

    public ArrayList<Integer> preventList;
    int start;
    int end;


    public Generate(int start, int end, ArrayList<Integer> preventList) {
        this.start = start;
        this.end = end;
        this.preventList = preventList;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    //метод который производит рандом
    public int getNextRandom() throws IllegalArgumentException {
        Random random1 = new Random();
        int x = end - start + 1 - preventList.size();
        int random = start + random1.nextInt(x);

        for (int exception : preventList) {
            if (random < exception) {
                break;
            }
            random++;

        }
        preventList.add(random);
        Collections.sort(preventList);

        return random;
    }
}



