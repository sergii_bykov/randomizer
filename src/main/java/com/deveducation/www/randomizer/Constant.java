package com.deveducation.www.randomizer;

public class Constant {
    static final String CMD_EXIT = "exit";
    static final String CMD_GENERATE = "generate";
    static final String CMD_HELP = "help";
    static final String CMD_SET_RANGE = "setRange";
    static final String CMD_SHOW_RANGE = "showRange";
    static final String MSG_RESET = "Reset to previous range";
    static final String MSG_STOP = "Program completed";
    static final String MSG_START = "Введите help - для вызова справки";
    static final String MSG_INVALID_COMMAND = "Invalid command";
    static final String MSG_LIST_COMMAND = "List of commands:\n showRange  \n setRange \n generate \n help \n exit ";
    static final String MSG_RANGE_ARE_SET = "The range are set at ";
    static final String MSG_SET_NEW_RANGE = "Set new range";
    static final String MSG_SET_START_RANGE = "Set START range";
    static final String MSG_SET_END_RANGE = "Set END range";
    static final String MSG_NEW_RANGE_ARE_SET = "New range are set at ";
    static final String MSG_OUT_OF_NUMBERS = "Out of numbers, select a new range ";
    static final String MSG_EXCEPTION = "----------------------------------- "+'\n'+"Range must be set as INT type"+'\n'+
            "START range< END range"+'\n'+"START must be than 1"+'\n'+"END must be than 500"+'\n'+
            "----------------------------------- "+'\n' ;
    static final String MSG_END = " and ";



}
