package com.deveducation.www.randomizer;

import java.util.ArrayList;
import java.util.Scanner;

import static com.deveducation.www.randomizer.Constant.*;


public class Command {
    private final Scanner scanner = new Scanner(System.in);

    private ArrayList<Integer> exclusionList = new ArrayList<Integer>(1);
    private Generate search = new Generate(1,1, exclusionList);


    public Command(){
        System.out.println(MSG_START);
        getNextCommand();
    }

    private void executedCommand(String command){
        switch (command){
            case CMD_EXIT:
                System.out.println(MSG_STOP);
                System.exit(0);
            case CMD_HELP:
                help();
                getNextCommand();
                break;
            case CMD_SHOW_RANGE:
                showBoundaries();
                getNextCommand();
                break;
            case CMD_SET_RANGE:
                range();
                getNextCommand();
                break;
            case CMD_GENERATE:
                generate();
                getNextCommand();
                break;
            default:
                System.out.println(MSG_INVALID_COMMAND);
                getNextCommand();
                break;
        }
    }

    private void help(){
        System.out.println(MSG_LIST_COMMAND);
    }

    private void showBoundaries(){
        System.out.println(MSG_RANGE_ARE_SET+ search.getStart()+ MSG_END+ search.getEnd()+".");

    }

    private void range(){
        System.out.println(MSG_SET_NEW_RANGE);
        exclusionList.clear();

        int tempEnd = search.getEnd();
        int tempStart = search.getStart();

        try{
            System.out.println(MSG_SET_START_RANGE);
            search.setStart(scanner.nextInt());
            int start = search.getStart();
            if(0 < start & start<501){
                search.setStart(start);

            }else{
                throw new Exception();
            }

            System.out.println(MSG_SET_END_RANGE);
            search.setEnd(scanner.nextInt());
            int end = search.getEnd();
            if(0< end & end<501){
                search.setEnd(end);
            }else{
                throw new Exception();
            }

            if(search.getStart()> search.getEnd()){
                throw new Exception();
            }
        }
        catch (Exception e){
            System.out.println(MSG_EXCEPTION);

            search.setStart(tempStart);
            search.setEnd(tempEnd);

            System.out.println(MSG_RESET);

        }
        System.out.println(MSG_NEW_RANGE_ARE_SET + search.getStart()+MSG_END+ search.getEnd()+".");
    }

    private void getNextCommand(){
        String nextCommand = scanner.next();
        executedCommand(nextCommand);
    }
    private void generate(){

        if(exclusionList.size()== search.getEnd()- search.getStart()+1){
            System.out.println(MSG_OUT_OF_NUMBERS);

        }else{
            int rnd = search.getNextRandom();
            System.out.println(search.preventList);
            System.out.println(rnd);

        }
    }


}


