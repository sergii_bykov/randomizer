package com.deveducation.www.randomizer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenerateTest {

    Generate cut;
    ArrayList<Integer> excludeList = new ArrayList<>(Arrays.asList(6, 7, 8));

    static List<Arguments> getNextRandomTestArgs() {
        return List.of(
                Arguments.arguments(6, 20),
                Arguments.arguments(1, 20)
        );
    }

    @ParameterizedTest
    @MethodSource("getNextRandomTestArgs")
    void getNextRandomTest(int start, int end) {
        cut = new Generate(start, end, excludeList);
        int actual = cut.getNextRandom();
        Assertions.assertTrue(true, String.valueOf(!excludeList.contains(actual)));
    }
}

